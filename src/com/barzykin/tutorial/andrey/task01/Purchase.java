package com.barzykin.tutorial.andrey.task01;

/**
 * @author alexej.barzykin@gmail.com
 */
public class Purchase {
    private float date;
    private String shopName;
    private String name;
    private String pan;
    private float cost;

    public float getDiscount() {
        return shopName.startsWith("S") ? 4 : 0;
    }

    public float getDate() {
        return date;
    }

    public void setDate(float date) {
        this.date = date;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public Purchase withDate(float date) {
        this.date = date;
        return this;
    }

    public Purchase witShopName(String shopName) {
        this.shopName = shopName;
        return this;
    }

    public Purchase withName(String name) {
        this.name = name;
        return this;
    }

    public Purchase withPan(String pan) {
        this.pan = pan;
        return this;
    }

    public Purchase withCost(float cost) {
        this.cost = cost;
        return this;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "date=" + date +
                ", shopName='" + shopName + '\'' +
                ", name='" + name + '\'' +
                ", pan='" + pan + '\'' +
                ", cost=" + cost +
                ", discount=" + getDiscount()
                + '}';
    }
}
