package com.barzykin.tutorial.andrey.task01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author alexej.barzykin@gmail.com
 */
public class Runner {
    private static final String HORIZONTAL_DELIMITER
            = "----------------------------------------------------------------------";

    public static void main(String[] args) {

        //прочитать входной файл;
        //создать массив структур (или список) и заполнить его данными из файла;
        List<Purchase> purchases = parseInputFile(args[0]);

        //вывести данные в порядке их чтения из файла
        purchases.forEach(System.out::println);
        printDelimiter();

        //упорядочить данные по дате и вывести данные (сначала более ранние покупки);
        purchases.stream()
                .sorted((pLeft, pRight) -> (int) (pLeft.getDate() - pRight.getDate()))
                .forEach(System.out::println);
        printDelimiter();

        // упорядочить данные по номеру кредитной карты,
        // подсчитать общую сумму покупок по каждой кредитной карте
        // вывести номер кредитной карты и общую сумму.
        purchases.sort(Comparator.comparing(Purchase::getPan));
        Purchase extPurchase = new Purchase();
        extPurchase.setPan(purchases.get(0).getPan());
        printPan(extPurchase);
        purchases.forEach(p -> {
            if (p.getPan().equals(extPurchase.getPan())) {
                extPurchase.setCost(extPurchase.getCost() + p.getCost());
            }
            else {
                printCost(extPurchase);
                extPurchase.setCost(p.getCost());
                extPurchase.setPan(p.getPan());
                printPan(extPurchase);
            }
        });
        printCost(extPurchase);
    }

    private static void printPan(Purchase extPurchase) {
        System.out.print(extPurchase.getPan() + " -> ");
    }

    private static void printCost(Purchase extPurchase) {
        System.out.println(extPurchase.getCost());
    }

    private static List<Purchase> parseInputFile(String arg) {
        List<Purchase> purchases = new ArrayList<>();
        try (Stream<String> lines = Files.lines(Paths.get(arg))) {
            purchases = lines.map(Runner::parsePurchase)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return purchases;
    }

    private static Purchase parsePurchase(String line) {
        String[] element = line.split(";");
        return new Purchase()
                .withDate(Float.parseFloat(element[0]))
                .witShopName(element[1])
                .withName(element[2])
                .withPan(element[3])
                .withCost(Float.parseFloat(element[4]));
    }

    private static void printDelimiter() {
        System.out.println(HORIZONTAL_DELIMITER);
        System.out.println();
    }
}